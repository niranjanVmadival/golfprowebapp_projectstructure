﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Facade
{
    internal static class ServiceURLs
    {
        public static string

        #region SignIn
           ProSignIn = "Identity/SignIn",
           ProSignUp = "Identity/SignUp",
        #endregion

        #region Profile

        #region Get
            GetProDetails = "Pro/GetProProfileInfo",
            GetExpertise = "Pro/GetAllExpertise",
            GetAgeLevels = "Pro/GetAllAgeLevels",
        #endregion

        #region Update
            UpdatePersonalInfo = "Pro/UpdatePersonalInfo",
            UpdateBankingInfo = "Pro/UpdateBankInfo",
            UpdatePriceInfo = "Pro/UpdatePricingInfo",
            UpdateTeachingStyle = "Pro/UpdatePreferredTeachingStyle",
        #endregion

        #endregion

        #region Supreme Golf
            GetSearchedCities = "search/cities",
            GetSearchedCourses = "search/courses",
            GetCourses = "courses";
        #endregion
    }
}
