﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Facade.DeploymentURLs
{
    public static class LocalServiceURL
    {
        #region Initialize Variables

        private static Dictionary<string, Uri> InternalService = new Dictionary<string, Uri>()
        {
            { "SignInService", new Uri("http://localhost:51007/api/")},
            { "ProService", new Uri("http://localhost:51253/api/")},
            { "SupremeGolfService", new Uri("https://staging.supremegolf.com:443/api/v4/")}
        };
        #endregion

        #region External Methods

        /// <summary>
        /// To Get Internal Service Path
        /// </summary>
        /// <param name="key">Key</param>
        /// <returns>Path</returns>
        public static Uri GetURI(string serviceKey)
        {
            try { return InternalService[serviceKey]; }
            catch (Exception ex) { throw ex; }
        }

        #endregion
    }
}
