﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Facade
{
    public static class Factory
    {
        #region Private Method
        private static IFacadeService _facade;
        #endregion

        //Factory of Golf Pro Service
        public static IFacadeService GetFacade(string environment)
        {
            //Singleton Pattern
            return _facade ?? _getFacade(environment);
        }

        //Private Methods
        #region Private Methods
        private static IFacadeService _getFacade(string environment)
        {
            //Assignment
            _facade = new FacadeService(environment);

            //Singleton Pattern
            return _facade;
        }
        #endregion
    }
}
