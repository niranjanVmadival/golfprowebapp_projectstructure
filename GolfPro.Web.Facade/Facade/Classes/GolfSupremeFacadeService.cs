﻿using GolfPro.Web.Common.Services.Utilities;
using GolfPro.Web.Facade.Facade.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace GolfPro.Web.Facade.Facade
{
    internal class GolfSupremeFacadeService : IGolfSupremeFacadeService
    {
        #region Initialize Variables 

        //Create Static URLs
        private readonly Type _classType;
        #endregion

        #region Constructor
        public GolfSupremeFacadeService(string environment)
        {
            //Get Class Name
            _classType = Type.GetType("GolfPro.Web.Facade.DeploymentURLs." + environment);
        }
        #endregion

        #region Create

        #endregion

        #region Read

        /// <summary>
        /// This Method is used to return list of Searched Cities
        /// </summary>
        /// <param name="queryString">Query string composed of 3 letters min.</param>
        /// <returns>The List of Searched Cities</returns>
        public Object GetSearchedCities(string queryString, string limit)
        {
            try
            {
                //Declaration
                Object _searchedCities = new Object();

                using (var _client = new HttpClient())
                {
                    //Set Base Address
                    _client.BaseAddress = GetServicePath("SupremeGolfService");
                    _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Receive response
                    HttpResponseMessage _response = _client.GetAsync(ServiceURLs.GetSearchedCities
                        .AddQueryParam("q", queryString)
                        .AddQueryParam("limit", limit)).Result;

                    //Validation
                    if (_response.IsSuccessStatusCode)
                    {
                        //Result conversion
                        _searchedCities = _response.Content.ReadAsJsonAsync<Object>().Result;
                    }
                    else
                    {
                        //Error Handler
                        var _errorAnynonmous = new { error = "" };

                        //Throw Error
                        throw new Exception(JsonConvert.DeserializeAnonymousType(_response.Content.ReadAsStringAsync().Result, _errorAnynonmous).error);
                    }

                    //Return
                    return _searchedCities;
                }
            }
            catch (Exception ex) { throw ex; }
        }


        public Object GetSearchedCourses(string queryString, string limit)
        {
            try
            {
                //Declaration
                Object _searchedCourses = new Object();

                using (var _client = new HttpClient())
                {
                    //Set Base Address
                    _client.BaseAddress = GetServicePath("SupremeGolfService");
                    _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Receive response
                    HttpResponseMessage _response = _client.GetAsync(ServiceURLs.GetSearchedCourses
                        .AddQueryParam("q", queryString)
                        .AddQueryParam("limit", limit)).Result;

                    //Validation
                    if (_response.IsSuccessStatusCode)
                    {
                        //Result conversion
                        _searchedCourses = _response.Content.ReadAsJsonAsync<Object>().Result;
                    }
                    else
                    {
                        //Error Handler
                        var _errorAnynonmous = new { error = "" };

                        //Throw Error
                        throw new Exception(JsonConvert.DeserializeAnonymousType(_response.Content.ReadAsStringAsync().Result, _errorAnynonmous).error);
                    }

                    //Return
                    return _searchedCourses;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public Object GetCourses(string pageNo, string pageLimit)
        {
            try
            {
                //Declaration
                Object _searchedCourses = new Object();

                using (var _client = new HttpClient())
                {
                    //Set Base Address
                    _client.BaseAddress = GetServicePath("SupremeGolfService");
                    _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    _client.DefaultRequestHeaders.Add("X-Api-Key", "3aa5397f-4de9-4dc3-9a04-ef92577832bb");

                    //Receive response
                    HttpResponseMessage _response = _client.GetAsync(ServiceURLs.GetCourses
                        .AddQueryParam("page", pageNo)
                        .AddQueryParam("per_page", pageLimit)).Result;

                    //Validation
                    if (_response.IsSuccessStatusCode)
                    {
                        //Result conversion
                        _searchedCourses = _response.Content.ReadAsJsonAsync<Object>().Result;
                    }
                    else
                    {
                        //Error Handler
                        var _errorAnynonmous = new { error = "" };

                        //Throw Error
                        throw new Exception(JsonConvert.DeserializeAnonymousType(_response.Content.ReadAsStringAsync().Result, _errorAnynonmous).error);
                    }

                    //Return
                    return _searchedCourses;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion

        #region Update

        #endregion

        #region Delete

        #endregion


        #region Service Methods
        /// <summary>
        /// To Get Service Path
        /// </summary>
        /// <param name="key">Key</param>
        /// <returns>Path</returns>
        private Uri GetServicePath(string serviceKey)
        {
            try
            {
                return (Uri)_classType.GetMethod("GetURI").Invoke(null, new object[] { serviceKey });
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion
    }
}
