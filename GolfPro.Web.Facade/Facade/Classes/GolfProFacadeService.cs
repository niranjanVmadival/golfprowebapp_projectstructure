﻿using GolfPro.Web.Common.Models;
using GolfPro.Web.Common.Models.Additional;
using GolfPro.Web.Common.Models.Profile;
using GolfPro.Web.Common.Models.Profile.DTO;
using GolfPro.Web.Common.Models.SignPro;
using GolfPro.Web.Common.Services.Utilities;
using GolfPro.Web.Facade.Facade.Interfaces;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;

namespace GolfPro.Web.Facade
{
    internal class GolfProFacadeService : IGolfProFacadeService
    {
        #region Initialize Variables 

        //Create Static URLs
        private readonly Type _classType;
        #endregion

        #region Constructor
        public GolfProFacadeService(string environment)
        {
            //Get Class Name
            _classType = Type.GetType("GolfPro.Web.Facade.DeploymentURLs." + environment);
        }
        #endregion

        #region Professional

        #region Create

        /// <summary>
        /// To Sign In
        /// </summary>
        /// <param name="email">Email of the User</param>
        /// <param name="password">Password of the User</param>
        /// <returns>The Token of User</returns>
        public AfterSignIn ProSignIn(SignPro profile)
        {
            try
            {
                //Declaration
                AfterSignIn _afterSignIn = new AfterSignIn();

                using (var _client = new HttpClient())
                {
                    //Creation of Dynamic variable
                    var _dynamic = new { email = profile.email, password = profile.password };

                    //Set Base Address
                    _client.BaseAddress = GetServicePath("SignInService");

                    //Receive response
                    HttpResponseMessage _response = _client.PostAsJsonAsync(ServiceURLs.ProSignIn, _dynamic).Result;

                    //Validation
                    if (_response.IsSuccessStatusCode)
                    {
                        //Result conversion
                        _afterSignIn = _response.Content.ReadAsJsonAsync<AfterSignIn>().Result;
                    }
                    else
                    {
                        //Error Handler
                        var _errorAnynonmous = new { error = "" };

                        //Throw Error
                        throw new Exception(JsonConvert.DeserializeAnonymousType(_response.Content.ReadAsStringAsync().Result, _errorAnynonmous).error);

                    }

                    //Return
                    return _afterSignIn;
                }
            }
            catch (Exception ex) { throw ex; }
        }


        /// <summary>
        /// To Sign Up a new Professional
        /// </summary>
        /// <param name="profile">Profile for Sign Up</param>
        /// <returns>Returns Status of Sign Up</returns>
        public AfterSignUp ProSignUp(SignPro profile)
        {
            try
            {
                //Declaration
                AfterSignUp _afterSignUp = new AfterSignUp();

                using (var _client = new HttpClient())
                {
                    //Set Base Address
                    _client.BaseAddress = GetServicePath("SignInService");

                    //Receive response
                    HttpResponseMessage _response = _client.PostAsJsonAsync(ServiceURLs.ProSignUp, profile).Result;

                    //Validation
                    if (_response.IsSuccessStatusCode)
                    {
                        //Result conversion
                        _afterSignUp = _response.Content.ReadAsJsonAsync<AfterSignUp>().Result;
                    }
                    else
                    {
                        //Error Handler
                        var _errorAnynonmous = new { error = "" };

                        //Throw Error
                        throw new Exception(JsonConvert.DeserializeAnonymousType(_response.Content.ReadAsStringAsync().Result, _errorAnynonmous).error);
                    }

                    //Return
                    return _afterSignUp;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion

        #region Read

        /// <summary>
        /// This Method is used to Get details of a Signed In Professional
        /// </summary>
        /// <param name="authToken">Authentication Token of s Pro</param>
        /// <returns>The Details of Signed In Pro</returns>
        public Object GetProDetails(string authToken)
        {
            try
            {
                //Declaration
                Object _proDetails = new Object();

                using (var _client = new HttpClient())
                {
                    //Set Base Address
                    _client.BaseAddress = GetServicePath("ProService");
                    _client.DefaultRequestHeaders.Add("X-Api-User-Token", authToken);

                    //Receive response
                    HttpResponseMessage _response = _client.GetAsync(ServiceURLs.GetProDetails).Result;

                    //Validation
                    if (_response.IsSuccessStatusCode)
                    {

                        //Error Handler
                        var _dynamic = new { pro =  new Professional() };

                        //Result conversion
                        _proDetails = JsonConvert.DeserializeAnonymousType(_response.Content.ReadAsStringAsync().Result, _dynamic).pro;
                    }
                    else
                    {
                        //Error Handler
                        var _errorAnynonmous = new { error = "" };

                        //Throw Error
                        throw new Exception(JsonConvert.DeserializeAnonymousType(_response.Content.ReadAsStringAsync().Result, _errorAnynonmous).error);
                    }

                    //Return
                    return _proDetails;
                }
            }
            catch (Exception ex) { throw ex; }
        }


        /// <summary>
        /// This Method is used to Get Expertise 
        /// </summary>
        /// <param name="authToken">Authentication Token of s Pro</param>
        /// <returns>The List of Expertise</returns>
        public Object GetExpertise(string authToken)
        {
            try
            {
                //Declaration
                Object _expertise = new Object();

                using (var _client = new HttpClient())
                {
                    //Set Base Address
                    _client.BaseAddress = GetServicePath("ProService");
                    _client.DefaultRequestHeaders.Add("X-Api-User-Token", authToken);

                    //Receive response
                    HttpResponseMessage _response = _client.GetAsync(ServiceURLs.GetExpertise).Result;

                    //Validation
                    if (_response.IsSuccessStatusCode)
                    {
                        //Result conversion
                        _expertise = _response.Content.ReadAsJsonAsync<Object>().Result;
                    }
                    else
                    {
                        //Error Handler
                        var _errorAnynonmous = new { error = "" };

                        //Throw Error
                        throw new Exception(JsonConvert.DeserializeAnonymousType(_response.Content.ReadAsStringAsync().Result, _errorAnynonmous).error);
                    }

                    //Return
                    return _expertise;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// This Method is used to Get Age Levels
        /// </summary>
        /// <param name="authToken">Authentication Token of s Pro</param>
        /// <returns>The List of Age Levels</returns>
        public Object GetAgeLevels(string authToken)
        {
            try
            {
                //Declaration
                Object _expertise = new Object();

                using (var _client = new HttpClient())
                {
                    //Set Base Address
                    _client.BaseAddress = GetServicePath("ProService");
                    _client.DefaultRequestHeaders.Add("X-Api-User-Token", authToken);

                    //Receive response
                    HttpResponseMessage _response = _client.GetAsync(ServiceURLs.GetAgeLevels).Result;

                    //Validation
                    if (_response.IsSuccessStatusCode)
                    {
                        //Result conversion
                        _expertise = _response.Content.ReadAsJsonAsync<Object>().Result;
                    }
                    else
                    {
                        //Error Handler
                        var _errorAnynonmous = new { error = "" };

                        //Throw Error
                        throw new Exception(JsonConvert.DeserializeAnonymousType(_response.Content.ReadAsStringAsync().Result, _errorAnynonmous).error);
                    }

                    //Return
                    return _expertise;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion

        #region Update

        /// <summary>
        /// This is to Update the Basic User Information of Professional
        /// </summary>
        /// <param name="userInformation">This is the Basic User Information of Professional</param>
        /// <returns>The Updated Professional Details</returns>
        public Object UpdateProUserDetails(string authToken, ProPersonalDTO userInformation)
        {
            try
            {
                //Declaration
                Object _proUserDetails = new Object();

                using (var _client = new HttpClient())
                {
                    //Set Base Address
                    _client.BaseAddress = GetServicePath("ProService");
                    _client.DefaultRequestHeaders.Add("X-Api-User-Token", authToken);

                    //Receive response
                    HttpResponseMessage _response = _client.PutJsonAsync(ServiceURLs.UpdatePersonalInfo, userInformation).Result;

                    //Validation
                    if (_response.IsSuccessStatusCode)
                    {
                        //Error Handler
                        var _dynamic = new { pro = new ProPersonalDTO() };

                        //Result conversion
                        _proUserDetails = JsonConvert.DeserializeAnonymousType(_response.Content.ReadAsStringAsync().Result, _dynamic).pro; 
                    }
                    else
                    {
                        //Error Handler
                        var _errorAnynonmous = new { error = "" };

                        //Throw Error
                        throw new Exception(JsonConvert.DeserializeAnonymousType(_response.Content.ReadAsStringAsync().Result, _errorAnynonmous).error);
                    }

                    //Return
                    return _proUserDetails;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// This is to Update the Banking Information of Professional
        /// </summary>
        /// <param name="userId">Id of a Professional</param>
        /// <param name="bankingInformation">This is the Banking Information of Professional</param>
        /// <returns>The Updated Professional Details</returns>
        public Object UpdateProBankingDetails(string userId, string authToken, BankInfo bankingInformation)
        {

            try
            {
                //Declaration
                Object _proBankingDetails = new Object();

                using (var _client = new HttpClient())
                {
                    //Set Base Address
                    _client.BaseAddress = GetServicePath("ProService");
                    _client.DefaultRequestHeaders.Add("X-Api-User-Token", authToken);

                    //Receive response
                    HttpResponseMessage _response = _client.PutJsonAsync(ServiceURLs.UpdateBankingInfo.AddQueryParam("userId", userId), bankingInformation).Result;

                    //Validation
                    if (_response.IsSuccessStatusCode)
                    {
                        //Result conversion
                        _proBankingDetails = _response.Content.ReadAsJsonAsync<Object>().Result;
                    }
                    else
                    {
                        //Error Handler
                        var _errorAnynonmous = new { error = "" };

                        //Throw Error
                        throw new Exception(JsonConvert.DeserializeAnonymousType(_response.Content.ReadAsStringAsync().Result, _errorAnynonmous).error);
                    }

                    //Return
                    return _proBankingDetails;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// This is to Update the Pricing Information of Professional
        /// </summary>
        /// <param name="userId">Id of a Professional</param>
        /// <param name="priceInformation">This is the Pricing Information of Professional</param>
        /// <returns>The Updated Professional Details</returns>
        public Object UpdateProPriceDetails(string userId, string authToken, PreferredPricingInfo priceInformation)
        {

            try
            {
                //Declaration
                Object _proPricingDetails = new Object();

                using (var _client = new HttpClient())
                {
                    //Set Base Address
                    _client.BaseAddress = GetServicePath("ProService");
                    _client.DefaultRequestHeaders.Add("X-Api-User-Token", authToken);

                    //Receive response
                    HttpResponseMessage _response = _client.PutJsonAsync(ServiceURLs.UpdatePriceInfo.AddQueryParam("userID", userId), priceInformation).Result;

                    //Validation
                    if (_response.IsSuccessStatusCode)
                    {
                        //Result conversion
                        _proPricingDetails = _response.Content.ReadAsJsonAsync<Object>().Result;
                    }
                    else
                    {
                        //Error Handler
                        var _errorAnynonmous = new { error = "" };

                        //Throw Error
                        throw new Exception(JsonConvert.DeserializeAnonymousType(_response.Content.ReadAsStringAsync().Result, _errorAnynonmous).error);
                    }

                    //Return
                    return _proPricingDetails;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// This is to Update the Teaching Information of a Professional
        /// </summary>
        /// <param name="userId">Id of a Professional</param>
        /// <param name="teachingInformation">Information related to Teaching Style</param>
        /// <returns></returns>
        public Object UpdateProTeachingDetails(string userId, string authToken, TeachingDTO teachingInformation)
        {

            try
            {
                //Declaration
                Object _proPricingDetails = new Object();

                using (var _client = new HttpClient())
                {
                    //Set Base Address
                    _client.BaseAddress = GetServicePath("ProService");
                    _client.DefaultRequestHeaders.Add("X-Api-User-Token", authToken);

                    //Receive response
                    HttpResponseMessage _response = _client.PutJsonAsync(ServiceURLs.UpdateTeachingStyle.AddQueryParam("userId", userId), teachingInformation).Result;

                    //Validation
                    if (_response.IsSuccessStatusCode)
                    {
                        //Result conversion
                        _proPricingDetails = _response.Content.ReadAsJsonAsync<Object>().Result;
                    }
                    else
                    {
                        //Error Handler
                        var _errorAnynonmous = new { error = "" };

                        //Throw Error
                        throw new Exception(JsonConvert.DeserializeAnonymousType(_response.Content.ReadAsStringAsync().Result, _errorAnynonmous).error);
                    }

                    //Return
                    return _proPricingDetails;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion

        #region Delete

        #endregion

        #endregion

        #region Service Methods
        /// <summary>
        /// To Get Service Path
        /// </summary>
        /// <param name="key">Key</param>
        /// <returns>Path</returns>
        private Uri GetServicePath(string serviceKey)
        {
            try
            {
                return (Uri)_classType.GetMethod("GetURI").Invoke(null, new object[] { serviceKey });
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion
    }
}