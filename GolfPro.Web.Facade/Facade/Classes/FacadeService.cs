﻿using GolfPro.Web.Facade.Facade;
using GolfPro.Web.Facade.Facade.Interfaces;
using System;

namespace GolfPro.Web.Facade
{
    internal class FacadeService : IFacadeService
    {
        #region Initialize Variables
        private readonly string _environment;
        #endregion

        #region Constructor
        public FacadeService()
        {

        }
        public FacadeService(string environment) : base()
        {
            //Assignment
            _environment = environment;
        }
        #endregion

        #region GolfProFacadeService
        public IGolfProFacadeService GolfProFacade()
        {
            return new GolfProFacadeService(_environment);
        }
        #endregion

        #region GolfSupremeFacadeService
        public IGolfSupremeFacadeService GolfSupremeFacade()
        {
            return new GolfSupremeFacadeService(_environment);
        }
        #endregion
    }
}
