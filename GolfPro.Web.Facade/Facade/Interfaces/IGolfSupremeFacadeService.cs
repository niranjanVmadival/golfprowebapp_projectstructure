﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Facade.Facade.Interfaces
{
    public interface IGolfSupremeFacadeService
    {

        #region Create

        #endregion

        #region Read

        //Searched Cities
        Object GetSearchedCities(string queryString, string limit);

        //Searched Courses
        Object GetSearchedCourses(string queryString, string limit);

        //Courses
        Object GetCourses(string pageNo, string pageLimit);

        #endregion

        #region Update

        #endregion

        #region Delete

        #endregion

    }
}
