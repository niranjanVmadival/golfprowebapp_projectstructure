﻿using GolfPro.Web.Common.Models.Additional;
using GolfPro.Web.Common.Models.Profile;
using GolfPro.Web.Common.Models.Profile.DTO;
using GolfPro.Web.Common.Models.SignPro;
using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Facade.Facade.Interfaces
{
    public interface IGolfProFacadeService
    {
        #region Professional

        #region Create

        #endregion

        #region Read

        //Pro Sign In
        AfterSignIn ProSignIn(SignPro profile);

        //Pro Sign Up
        AfterSignUp ProSignUp(SignPro profile);

        //Pro Details
        Object GetProDetails(string authToken);

        //Pro Expertise
        Object GetExpertise(string authToken);

        //Pro Age Levels
        Object GetAgeLevels(string authToken);

        #endregion

        #region Update

        //Pro User Information Details
        Object UpdateProUserDetails(string authToken, ProPersonalDTO userInformation);

        //Pro User Banking Details
        Object UpdateProBankingDetails(string userId, string authToken, BankInfo bankingInformation);

        //Pro User Pricing Details
        Object UpdateProPriceDetails(string userId, string authToken, PreferredPricingInfo pricingInformation);

        //Pro User Teaching Details 
        Object UpdateProTeachingDetails(string userId, string authToken, TeachingDTO teachingInformation);

        #endregion

        #region Delete

        #endregion

        #endregion

    }
}
