﻿using GolfPro.Web.Facade.Facade.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Facade
{
    public interface IFacadeService
    {
        //Golf Pro Service
        IGolfProFacadeService GolfProFacade();

        //Golf Supreme Service
        IGolfSupremeFacadeService GolfSupremeFacade();

    }
}
