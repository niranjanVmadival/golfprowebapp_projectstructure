﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace GolfPro.Web.Resources.Encryption
{
    public class EncryptionService
    {

        #region Initialize Variables
        private readonly IConversion _encrypt = new Encrypt();
        private readonly IConversion _decrypt = new Decrypt();

        //Private Key for Encryption
        public static string EncryptionKey = "$9P0R5A8T7I2N";

        //Byte Encryption
        public static byte[] Bytes = new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 };
        #endregion

        //Encryption
        public string Encrypt(string clearText)
        {
            try { return _encrypt.Conversion(Encoding.Unicode.GetBytes(clearText)); }
            catch (Exception ex) { throw ex; }
        }

        //Decryption
        public string Decrypt(string cipherText)
        {
            try { return _decrypt.Conversion(Convert.FromBase64String(cipherText)); }
            catch (Exception ex) { throw ex; }
        }

        #region Helper Methods

        /// <summary>
        /// To Derive Bytes
        /// </summary>
        /// <param name="encryptor">Encryptor AES</param>
        /// <returns>AES</returns>
        public static Aes DeriveBytes(Aes encryptor)
        {
            try
            {
                //Creation
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, Bytes);
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);

                //Return
                return encryptor;
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion
    }

    #region Interface segregation principle
    public interface IConversion
    {
        string Conversion(Byte[] bytes);
    }
    public class Encrypt : IConversion
    {
        public string Conversion(Byte[] bytes)
        {
            try
            {
                //Use AES Encryption
                Aes encryptor = Aes.Create();

                //CryptoGraphy             
                encryptor = EncryptionService.DeriveBytes(encryptor);

                //Check the Mode
                ICryptoTransform _conversion = encryptor.CreateEncryptor();

                //Memory Stream
                using (MemoryStream _memoryStr = new MemoryStream())
                {
                    //Crypto Stream
                    using (CryptoStream _cryptoStr = new CryptoStream(_memoryStr, _conversion, CryptoStreamMode.Write))
                    {
                        _cryptoStr.Write(bytes, 0, bytes.Length);
                        _cryptoStr.Close();
                    }

                    //Return
                    return Convert.ToBase64String(_memoryStr.ToArray());
                }
            }
            catch (Exception ex) { throw ex; }
        }
    }
    public class Decrypt : IConversion
    {
        public string Conversion(byte[] bytes)
        {
            try
            {
                //Use AES Encryption
                Aes encryptor = Aes.Create();

                //CryptoGraphy             
                encryptor = EncryptionService.DeriveBytes(encryptor);

                //Check the Mode
                ICryptoTransform _conversion = encryptor.CreateDecryptor();

                //Memory Stream
                using (MemoryStream _memoryStr = new MemoryStream())
                {
                    //Crypto Stream
                    using (CryptoStream _cryptoStr = new CryptoStream(_memoryStr, _conversion, CryptoStreamMode.Write))
                    {
                        _cryptoStr.Write(bytes, 0, bytes.Length);
                        _cryptoStr.Close();
                    }

                    //Return
                    return Encoding.Unicode.GetString(_memoryStr.ToArray());
                }
            }
            catch (Exception ex) { throw ex; }
        }
    }
    #endregion
}
