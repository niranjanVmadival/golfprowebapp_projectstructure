﻿using GolfPro.Web.Facade;
using GolfPro.Web.Resources.Encryption;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace GolfPro.Web.Resources.Session
{
    public class FactoryService
    {
        #region Initialize Variables
        private readonly IFacadeService _facade;
        private readonly IConfiguration _configuration;
        private readonly EncryptionService _cryption;
        #endregion

        #region Constructor
        public FactoryService(IConfiguration configuration)
        {
            //Assignment
            _configuration = configuration;

            //Services
            _facade = Factory.GetFacade(_configuration["Urls"]);
            _cryption = new EncryptionService();
        } 
        #endregion

        #region Services Instances

        #region Public Methods
        public IFacadeService GetFacadeService() { return _facade; }

        public EncryptionService GetCryptionService() { return _cryption; }
        #endregion

        #endregion


    }
}
