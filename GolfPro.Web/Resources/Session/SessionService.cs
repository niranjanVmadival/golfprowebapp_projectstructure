﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace GolfPro.Web.Resources.Session
{
    public static class SessionService
    {
        #region Initialize Variables
        
        #endregion

        #region Private Methods

        #region User Session Token
        private static void _setSessionToken(string token, HttpContext httpContext)
        {
            try { httpContext.Session.SetString("authToken", token); }
            catch (Exception ex) { throw ex; }
        }

        private static string _getSessionToken(HttpContext httpContext)
        {
            try { return httpContext.Session.GetString("authToken"); }
            catch (Exception ex) { throw ex; }
        }
        #endregion

        #endregion

        #region Public Methods
        public static void SetSessionToken(string token, HttpContext httpContext)
        {
            try { _setSessionToken(token, httpContext); }
            catch (Exception ex) { throw ex; }
        }

        public static string GetSessionToken(HttpContext httpContext)
        {
            try { return _getSessionToken(httpContext); }
            catch (Exception ex) { throw ex; }
        }
        #endregion
    }
}
