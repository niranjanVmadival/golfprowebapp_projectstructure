﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfPro.Web.Resources.Message
{
    public static class MessageService
    {
        #region Initialize Variables
        private static string
            ErrorMsg = "UnSuccessful Attempt",
            SuccessMsg = "Successfully ";
        #endregion

        #region Public Methods
        public static string Error()
        {
            try { return ErrorMsg; }
            catch (Exception ex) { throw ex; }
        }

        public static string Success(string message)
        {
            try { return (SuccessMsg + message); }
            catch (Exception ex) { throw ex; }
        }
        #endregion
    }
}
