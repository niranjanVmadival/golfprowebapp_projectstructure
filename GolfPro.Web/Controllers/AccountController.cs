﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using GolfPro.Web.Resources;
using Microsoft.AspNetCore.Mvc;
using GolfPro.Web.Facade;
using Microsoft.Extensions.Configuration;
using System.Configuration.Abstractions;
using GolfPro.Web.Resources.Session;
using GolfPro.Web.Resources.Encryption;
using GolfPro.Web.Common.Models;
using GolfPro.Web.Common.Models.Additional;
using GolfPro.Web.Common.Models.Common;
using GolfPro.Web.Resources.Message;
using GolfPro.Web.Common.Models.SignPro;

namespace GolfPro.Web.Controllers
{
    public class AccountController : Controller
    {
        #region Initialize Variables
        private readonly FactoryService _factory;
        private readonly EncryptionService _cryption;
        private readonly IFacadeService _facade;
        private static JsonData _jsonData;
        #endregion

        #region Constructor
        public AccountController(IConfiguration configuration)
        {
            //Assignment
            _factory = new FactoryService(configuration);
            _jsonData = new JsonData();

            //Services
            _facade = _factory.GetFacadeService();
            _cryption = _factory.GetCryptionService();
        }
        #endregion

        #region Views
        public IActionResult Index()
        {
            return View();
        }
        #endregion

        #region Professional

        [HttpPost]
        /// <summary>
        /// To Sign In
        /// </summary>
        /// <param name="email">Email of the User</param>
        /// <param name="password">Password of the User</param>
        /// <returns>The Token of User</returns>
        public JsonResult ProSignIn([FromBody] SignPro profile)
        {
            //Variable Declaration
            AfterSignIn _afterSignIn;

            try
            {
                //Encrypt the Password
                //string _password = _cryption.Encrypt(password);

                //Call the Facade
                _afterSignIn = _facade.GolfProFacade().ProSignIn(profile);

                //Storage of Token in Session
                SessionService.SetSessionToken(_afterSignIn.token.ToString(), HttpContext);

                //Set Message
                _jsonData.Message = MessageService.Success("Signed In");

                //Set Object
                _jsonData.Object = _afterSignIn;
            }
            catch (Exception ex)
            {
                //Set Exception
                _jsonData.Exception = ex.Message;

                //Set Message
                _jsonData.Message = MessageService.Error();
            }
            finally
            { }

            //Return
            return Json(_jsonData);
        }

        [HttpPost]
        /// <summary>
        /// To Sign Up a new Professional
        /// </summary>
        /// <param name="profile">Profile for Sign Up</param>
        /// <returns>Returns Status of Sign Up</returns>
        public JsonResult ProSignUp([FromBody] SignPro profile)
        {
            //Variable Declaration
            AfterSignUp _afterSignUp;

            //Encrypt Password
            //profile.password = _cryption.Encrypt(profile.password);

            try
            {
                //Call the Facade
                _afterSignUp = _facade.GolfProFacade().ProSignUp(profile);

                //Storage of Token in Session
                SessionService.SetSessionToken(_afterSignUp.token.ToString(), HttpContext);

                //Set Message
                _jsonData.Message = MessageService.Success("signed up the Professional");

                //Set Object
                _jsonData.Object = _afterSignUp;
            }
            catch (Exception ex)
            {
                //Set Exception
                _jsonData.Exception = ex.Message;

                //Set Message
                _jsonData.Message = MessageService.Error();
            }
            finally
            { }

            //Return
            return Json(_jsonData);
        }

        #endregion

    }
}