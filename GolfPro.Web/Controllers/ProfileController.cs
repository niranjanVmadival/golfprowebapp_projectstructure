﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GolfPro.Web.Common.Models.Common;
using GolfPro.Web.Common.Models.Additional;
using GolfPro.Web.Facade;
using GolfPro.Web.Resources.Message;
using GolfPro.Web.Resources.Session;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using GolfPro.Web.Common.Models.Profile;
using GolfPro.Web.Common.Models.Profile.DTO;

namespace GolfPro.Web.Controllers
{
    public class ProfileController : Controller
    {

        #region Initialize Variables
        private readonly IFacadeService _facade;
        private readonly FactoryService _factory;
        private static JsonData _jsonData;
        #endregion

        #region Constructor
        public ProfileController(IConfiguration configuration)
        {
            //Assignment
            _factory = new FactoryService(configuration);
            _jsonData = new JsonData();

            //Services
            _facade = _factory.GetFacadeService();
        }
        #endregion

        #region Views
        public IActionResult Index()
        {
            return View();
        }
        #endregion

        #region Professional

        #region Create

        #endregion

        #region Read
        [HttpGet]
        /// <summary>
        /// This Method is used to Get details of a Signed In Professional
        /// </summary>
        /// <returns>The Details of Signed In Pro</returns>
        public JsonResult GetProDetails()
        {
            //Variable Declaration
            Object _object;

            try
            {
                //Call the Facade
                _object = _facade.GolfProFacade().GetProDetails(SessionService.GetSessionToken(HttpContext));

                //Set Message
                _jsonData.Message = MessageService.Success("retrieved the Pro Details");

                //Set Object
                _jsonData.Object = _object;
            }
            catch (Exception ex)
            {
                //Set Exception
                _jsonData.Exception = ex.Message;

                //Set Message
                _jsonData.Message = MessageService.Error();
            }
            finally
            { }

            //Return
            return Json(_jsonData);
        }

        /// <summary>
        /// This Method is used to Get Expertise 
        /// </summary>
        /// <returns>The List of Expertise</returns>
        public JsonResult GetExpertise()
        {
            //Variable Declaration
            Object _object;

            try
            {
                //Call the Facade
                _object = _facade.GolfProFacade().GetExpertise(SessionService.GetSessionToken(HttpContext));

                //Set Message
                _jsonData.Message = MessageService.Success("retrieved the list of Expertise");

                //Set Object
                _jsonData.Object = _object;
            }
            catch (Exception ex)
            {
                //Set Exception
                _jsonData.Exception = ex.Message;

                //Set Message
                _jsonData.Message = MessageService.Error();
            }
            finally
            { }

            //Return
            return Json(_jsonData);
        }

        /// <summary>
        /// This Method is used to Get Age Levels
        /// </summary>
        /// <returns>The List of Age Levels</returns>
        public JsonResult GetAgeLevels()
        {
            //Variable Declaration
            Object _object;

            try
            {
                //Call the Facade
                _object = _facade.GolfProFacade().GetAgeLevels(SessionService.GetSessionToken(HttpContext));

                //Set Message
                _jsonData.Message = MessageService.Success("retrieved the list of Age Levels");

                //Set Object
                _jsonData.Object = _object;
            }
            catch (Exception ex)
            {
                //Set Exception
                _jsonData.Exception = ex.Message;

                //Set Message
                _jsonData.Message = MessageService.Error();
            }
            finally
            { }

            //Return
            return Json(_jsonData);
        }
        #endregion

        #region Update
        [HttpPut]
        /// <summary>
        /// This is to Update the Basic User Information of Professional
        /// </summary>
        /// <param name="userInformation">This is the Basic User Information of Professional</param>
        /// <returns>The Updated Professional Details</returns>
        public JsonResult UpdateProUserDetails([FromBody]ProPersonalDTO userInformation)
        {
            //Variable Declaration
            Object _object;

            try
            {
                //Call the Facade
                _object = _facade.GolfProFacade().UpdateProUserDetails(SessionService.GetSessionToken(HttpContext), userInformation);

                //Set Message
                _jsonData.Message = MessageService.Success("updated the Pro User Details");

                //Set Object
                _jsonData.Object = _object;
            }
            catch (Exception ex)
            {
                //Set Exception
                _jsonData.Exception = ex.Message;

                //Set Message
                _jsonData.Message = MessageService.Error();
            }
            finally
            { }

            //Return
            return Json(_jsonData);
        }

        [HttpPut]
        /// <summary>
        /// This is to Update the Banking Information of Professional
        /// </summary>
        /// <param name="userId">Id of a Professional</param>
        /// <param name="bankingInformation">This is the Banking Information of Professional</param>
        /// <returns>The Updated Professional Details</returns>
        public JsonResult UpdateProBankingDetails(string userId, [FromBody]BankInfo bankingInformation)
        {
            //Variable Declaration
            Object _object;

            try
            {
                //Call the Facade
                _object = _facade.GolfProFacade().UpdateProBankingDetails(userId, SessionService.GetSessionToken(HttpContext), bankingInformation);

                //Set Message
                _jsonData.Message = MessageService.Success("updated the Pro Banking Details");

                //Set Object
                _jsonData.Object = _object;
            }
            catch (Exception ex)
            {
                //Set Exception
                _jsonData.Exception = ex.Message;

                //Set Message
                _jsonData.Message = MessageService.Error();
            }
            finally
            { }

            //Return
            return Json(_jsonData);
        }

        [HttpPut]
        /// <summary>
        /// This is to Update the Pricing Information of Professional
        /// </summary>
        /// <param name="userId">Id of a Professional</param>
        /// <param name="priceInformation">This is the Pricing Information of Professional</param>
        /// <returns>The Updated Professional Details</returns>
        public JsonResult UpdateProPriceDetails(string userId, [FromBody] PreferredPricingInfo pricingInformation)
        {
            //Variable Declaration
            Object _object;

            try
            {
                //Call the Facade
                pricingInformation.CurrencyInfoID = 1;
                _object = _facade.GolfProFacade().UpdateProPriceDetails(userId, SessionService.GetSessionToken(HttpContext), pricingInformation);

                //Set Message
                _jsonData.Message = MessageService.Success("updated the Pro Price Details");

                //Set Object
                _jsonData.Object = _object;
            }
            catch (Exception ex)
            {
                //Set Exception
                _jsonData.Exception = ex.Message;

                //Set Message
                _jsonData.Message = MessageService.Error();
            }
            finally
            { }

            //Return
            return Json(_jsonData);
        }

        [HttpPut]
        /// <summary>
        /// This is to Update the Teaching Information of a Professional
        /// </summary>
        /// <param name="userId">Id of a Professional</param>
        /// <param name="teachingInformation">Information related to Teaching Style</param>
        /// <returns></returns>
        public JsonResult UpdateProTeachingDetails(string userId, [FromBody] TeachingDTO teachingInformation)
        {
            //Variable Declaration
            Object _object;

            try
            {
                //Call the Facade
                _object = _facade.GolfProFacade().UpdateProTeachingDetails(userId, SessionService.GetSessionToken(HttpContext), teachingInformation);

                //Set Message
                _jsonData.Message = MessageService.Success("updated the Pro Teaching Details");

                //Set Object
                _jsonData.Object = _object;
            }
            catch (Exception ex)
            {
                //Set Exception
                _jsonData.Exception = ex.Message;

                //Set Message
                _jsonData.Message = MessageService.Error();
            }
            finally
            { }

            //Return
            return Json(_jsonData);
        }
        #endregion

        #region Delete

        #endregion

        #endregion
    }
}