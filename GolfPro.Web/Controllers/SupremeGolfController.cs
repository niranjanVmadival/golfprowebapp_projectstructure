﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GolfPro.Web.Common.Models.Common;
using GolfPro.Web.Facade;
using GolfPro.Web.Resources.Message;
using GolfPro.Web.Resources.Session;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace GolfPro.Web.Controllers
{
    public class SupremeGolfController : Controller
    {

        #region Initialize Variables
        private readonly IFacadeService _facade;
        private readonly FactoryService _factory;
        private static JsonData _jsonData;
        #endregion

        #region Constructor
        public SupremeGolfController(IConfiguration configuration)
        {
            //Assignment
            _factory = new FactoryService(configuration);
            _jsonData = new JsonData();

            //Services
            _facade = _factory.GetFacadeService();
        }
        #endregion

        #region Views
        public IActionResult Index()
        {
            return View();
        }
        #endregion

        #region Supreme Golf Methods

        #region Create

        #endregion

        #region Read
        [HttpGet]
        /// <summary>
        /// This Method is used to return list of Searched Cities
        /// </summary>
        /// <param name="queryString">Query string composed of 3 letters min.</param>
        /// <returns>The List of Searched Cities</returns>
        public JsonResult GetSearchedCities(string queryString, string limit)
        {
            //Variable Declaration
            Object _object;

            try
            {
                //Call the Facade
                _object = _facade.GolfSupremeFacade().GetSearchedCities(queryString, limit);

                //Set Message
                _jsonData.Message = MessageService.Success("retrieved the list of searched Cities");

                //Set Object
                _jsonData.Object = _object;
            }
            catch (Exception ex)
            {
                //Set Exception
                _jsonData.Exception = ex.Message;

                //Set Message
                _jsonData.Message = MessageService.Error();
            }
            finally
            { }

            //Return
            return Json(_jsonData);
        }

        [HttpGet]
        /// <summary>
        /// This Method is used to return list of Searched Courses
        /// </summary>
        /// <param name="queryString">Query string composed of 3 letters min.</param>
        /// <returns>The List of Searched Courses</returns>
        /// <returns></returns>
        public JsonResult GetSearchedCourses(string queryString, string limit)
        {
            //Variable Declaration
            Object _object;

            try
            {
                //Call the Facade
                _object = _facade.GolfSupremeFacade().GetSearchedCourses(queryString, limit);

                //Set Message
                _jsonData.Message = MessageService.Success("retrieved the list of searched Courses");

                //Set Object
                _jsonData.Object = _object;
            }
            catch (Exception ex)
            {
                //Set Exception
                _jsonData.Exception = ex.Message;

                //Set Message
                _jsonData.Message = MessageService.Error();
            }
            finally
            { }

            //Return
            return Json(_jsonData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageNo"></param>
        /// <param name="pageLimit"></param>
        /// <returns></returns>
        public JsonResult GetCourses(string pageNo, string pageLimit)
        {
            //Variable Declaration
            Object _object;

            try
            {
                //Call the Facade
                _object = _facade.GolfSupremeFacade().GetCourses(pageNo, pageLimit);

                //Set Message
                _jsonData.Message = MessageService.Success("retrieved the list of Courses");

                //Set Object
                _jsonData.Object = _object;
            }
            catch (Exception ex)
            {
                //Set Exception
                _jsonData.Exception = ex.Message;

                //Set Message
                _jsonData.Message = MessageService.Error();
            }
            finally
            { }

            //Return
            return Json(_jsonData);
        }

        #endregion

        #region Update

        #endregion

        #region Delete

        #endregion

        #endregion
    }
}