/*
 * Component  : App
 * Created by : Niranjan V
 * Created on : 17th May 2018
 */

//Imports
import { Component } from '@angular/core';

//Component
@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})

//Class
export class AppComponent { }
