﻿/*
 * Component  : Profile
 * Created by : Niranjan V
 * Created on : 16th May 2018
 */

//Imports
import { Component, OnInit } from '@angular/core';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

//Services
import { SessionService } from '../services/app.session.service';
import { HttpService } from '../services/app.http.service';
import { ClassService } from '../services/app.classes.service';
import { forEach } from '@angular/router/src/utils/collection';

//Components
@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css'],
    providers: [
        AngularFontAwesomeModule,
        SessionService, HttpService, ClassService                               //Global Services
    ]
})

//Class
export class ProfileComponent {

    //Variable Declaration
    public professional: any = {};

    public loading: boolean = true;

    public addelement: any = function (ind: any, type: string) { }
    public removeelement: any = function (ind: any, type: string) { }

    public availability: any = {
        isExpertiseNotAvailable: false,
        isAgeLevelsNotAvailable: false,
        isAccoladesNotAvailable: false,
        isNotableplayerscoachedNotAvailable: false,
        isBankInfoNotAvailable: false,
        isRatesNotAvailable: false
    };

    public expertise: any = [];
    public ageLevels: any = [];
    public countries: any = [];
    public courses: any = [];

    private saveDetails: any = {};
    private editDetails: any = {};

    public userSection: any = {
        "name": "Name",
        "email": "Email",
        "phone": "Phone",
        "address": "Address",
        "country": "Country",
        "contact": "Contact",
        "birthday": "Birthday",
        "gender": "Gender",
        "demographics": "Courses",
        "description": "Description",

        //View/Edit Mode
        viewMode: true,

        //Temporary Object
        temp: {}
    };

    public teachingStyleSection: any = {
        "expertise": {
            "name": "Expertise",
            "nodata": "You haven't provided your expertise level yet."
        },
        "ageLevels": {
            "name": "Age Levels To Coach",
            "nodata": "You haven't provided the age levels you can coach."
        },
        "accolades": {
            "name": "Accolades",
            "nodata": "Please list down your achievements."
        },
        "players": {
            "name": "Notable Players Coached",
            "nodata": "Please list down the notable players you have coached."
        },
        "pga": {
            "name": "PGA Certified"
        },

        //View/Edit Mode
        viewMode: true,

        //Temporary Object
        temp: {}
    };

    public bankingSection: any = {
        "name": "Name",
        "acctype": "Account Type",
        "routing": "Routing Number",
        "account": "Account Number",
        "nodata": "You haven't provided your Banking details yet.",

        //View/Edit Mode
        viewMode: true,

        //Temporary Object
        temp: {}
    };

    public pricingSection: any = {
        "individual": "Individual",
        "group": "Group",
        "package": "Package",

        "nodata": "You haven't provided your rates yet.",

        //View/Edit Mode
        viewMode: true,

        //Temporary Object
        temp: {}
    };

    //Constructor
    constructor(
        private _sessionService: SessionService,
        private _classService: ClassService,
        private _httpService: HttpService
    ) {

        //Global Reference
        var self = this;

        self.addelement = function (ind: any, type: string) {

            var _object = (type == "notablePlayers") ?
                { "id": 0, "firstname": "", "lastname": "" } : { "id": 0, "title": "" };

            self.teachingStyleSection.temp[type].push(_object);
        }

        self.removeelement = function (ind: any, type: string) {
            (self.teachingStyleSection.temp[type].length != 1) ? self.teachingStyleSection.temp[type].splice(ind, 1) : "";
        }

        //Get Profile Details
        this._httpService.getProDetails()
            .then((res: any, msg: any) => {
                try {

                    //var _mockResponse = {
                    //    accolades: [],
                    //    bankinginfo: [],
                    //    notableplayerscoached: []
                    //};
                    //this.professional = Object.assign(res.object, _mockResponse);

                    this.professional = res.object;

                    //Handle Null or Undefined Scenarios
                    //var _nullArr = [
                    //    { "accolades": {} },
                    //    { "notableplayerscoached": {} },
                    //    { "bankinginfo": {} },
                    //    { "teachingstyle": { "expertiselevels": [], "teachingstyle.agelevels": [] } }];

                    //_nullArr.forEach(function (key: any, index: any) {
                    //    key.forEach(function (attr: any, ind: any) {
                    //        self.professional[attr] = (self.professional[attr] == null || self.professional[attr] == undefined) ? attr[ind] : self.professional[attr]
                    //    });
                    //});
                    this.professional.accolades = (this.professional.accolades === null || this.professional.accolades === undefined) ? [] : this.professional.accolades;
                    this.professional.notableplayerscoached = (this.professional.notableplayerscoached === null || this.professional.notableplayerscoached === undefined) ? [] : this.professional.notableplayerscoached;
                    this.professional.bankinginfo = (this.professional.bankinginfo === null || this.professional.bankinginfo === undefined) ? [] : this.professional.bankinginfo;
                    this.professional.teachingstyle = (this.professional.teachingstyle === null || this.professional.teachingstyle === undefined) ? { expertiselevels: [], agelevels: [] } : this.professional.teachingstyle;

                    //Banking Info mapped as 1 Account per User
                    this.professional.bankinginfo = (this.professional.bankinginfo.length != 0) ? this.professional.bankinginfo[0] : [];
                    console.log(this.professional);

                    //Handle Null Data Scenario
                    this.availability.isExpertiseNotAvailable = (this.professional.teachingstyle.expertiselevels.length == 0);
                    this.availability.isAgeLevelsNotAvailable = (this.professional.teachingstyle.agelevels.length == 0);
                    this.availability.isAccoladesNotAvailable = (this.professional.accolades.length == 0);
                    this.availability.isNotableplayerscoachedNotAvailable = (this.professional.notableplayerscoached.length == 0);
                    this.availability.isBankInfoNotAvailable = (this.professional.bankinginfo.length == 0);
                    this.availability.isRatesNotAvailable = (this.professional.rates === null);


                    this._httpService.getExpertise()
                        .then((res: any, msg: any) => {
                            try {
                                this.expertise = res.object;

                                self.expertise.forEach(function (valOne: any, indOne: number) {
                                    self.professional.teachingstyle.expertiselevels.forEach(function (valTwo: any, indTwo: any) {
                                        (valOne.id == valTwo.expertiseid) ? valTwo.title = valOne.title : "";
                                    })
                                })
                            }
                            catch (e) { throw e; }
                        });

                    this._httpService.getAgeLevels()
                        .then((res: any, msg: any) => {
                            try {
                                this.ageLevels = res.object;

                                self.ageLevels.forEach(function (valOne: any, indOne: number) {
                                    self.professional.teachingstyle.agelevels.forEach(function (valTwo: any, indTwo: any) {
                                        (valOne.id == valTwo.agelevelid) ? valTwo.level = valOne.level : "";
                                    })
                                })
                            }
                            catch (e) { throw e; }
                        });

                    this._httpService.GetCountries()
                        .then((res: any, msg: any) => {
                            try { this.countries = res.countries; }
                            catch (e) { throw e; }
                        });

                    this._httpService.GetCourses()
                        .then((res: any, msg: any) => {
                            try {
                                this.courses = res.object.courses;

                                self.courses.forEach(function (valOne: any, indOne: any) {
                                    (self.professional.courseid == valOne.id) ? self.professional.coursename = valOne.name : "";
                                })
                            }
                            catch (e) { throw e; }
                        });

                    this.loading = false;
                }
                catch (e) { throw e; }
            });

        //this._httpService.GetSearchedCities("los")
        //    .then((res: any, msg: any) => {
        //        console.log(res);
        //    });

        //Save Details
        this.saveDetails = {

            //To Save Basic Details of a Pro
            basicInfo: function (professional: any, userSection: any) {
                try {

                    var _referenceObj = {
                        id: 0,
                        personal: {
                            "firstname": "",
                            "lastname": "",
                            "email": "",
                            "phone": ""
                        },
                        address: {
                            "address_1": "",
                            "address_2": "",
                            "city": "",
                            "state": "",
                            "zip": "",
                            "country": ""
                        },
                        dob: "",
                        courseid: "",
                        gender: "",
                        prodescription: ""
                    };

                    //Set Personal details
                    _referenceObj.personal = userSection.temp.personal;

                    //Set Address details
                    _referenceObj.address = userSection.temp.address;

                    //Set Date of Birth
                    _referenceObj.dob = userSection.temp.dob;

                    //Set Course Id 
                    _referenceObj.courseid = userSection.temp.courseid;

                    //Set Gender
                    _referenceObj.gender = userSection.temp.gender;

                    //Set Id
                    _referenceObj.id = userSection.temp.id;

                    //Set Description
                    _referenceObj.prodescription = userSection.temp.prodescription;

                    self._httpService.updateProUserDetails(_referenceObj)
                        .then((res: any, msg: any) => {
                            try {

                                let _error: any = function () {
                                    try { alert(res.exception); }
                                    catch (e) { throw e; }
                                };

                                let _success: any = function () {
                                    try {
                                        //Check the State
                                        professional = Object.assign(professional, _referenceObj);

                                        //Enable back the View Mode
                                        userSection.viewMode = true;
                                    }
                                    catch (e) { throw e; }
                                };

                                (res.exception) ? _error() : _success();

                            }
                            catch (e) { throw e; }
                        });
                }
                catch (e) { throw e; }
            },

            //To Save Banking Details of a Pro
            bankInfo: function (professional: any, bankingSection: any) {
                try {

                    let _assign: any = function () {
                        //Assign Values to Main Reference
                        professional.bankinginfo = bankingSection.temp;
                    }

                    let _merge: any = function () {
                        //Assign Values to Main Reference
                        professional.bankinginfo = Object.assign(professional.bankinginfo, bankingSection.temp);
                    }

                    self._httpService.updateProBankingDetails(professional.id, bankingSection.temp)
                        .then((res: any, msg: any) => {
                            try {

                                let _error: any = function () {
                                    try { alert(res.message); }
                                    catch (e) { throw e; }
                                };

                                let _success: any = function () {
                                    try {

                                        //Check the State
                                        (professional.bankinginfo === null) ? _assign() : _merge();

                                        //Enable back the View Mode
                                        bankingSection.viewMode = true;

                                        //Enable the Data
                                        self.availability.isBankInfoNotAvailable = false;
                                    }
                                    catch (e) { throw e; }
                                };

                                (res.exception) ? _error() : _success();
                            }
                            catch (e) { throw e; }
                        });
                }
                catch (e) { throw e; }
            },

            //To Save Pricing Details of a Pro
            priceInfo: function (professional: any, pricingSection: any) {
                try {

                    let _assign: any = function () {
                        //Assign Values to Main Reference
                        professional.rates = pricingSection.temp;
                    }

                    let _merge: any = function () {
                        //Assign Values to Main Reference
                        professional.rates = Object.assign(professional.rates, pricingSection.temp);
                    }

                    //Make HTTP Call to save data
                    self._httpService.updateProPriceDetails(professional.id, pricingSection.temp)
                        .then((res: any, msg: any) => {
                            try {

                                //Check the State
                                (professional.rates === null) ? _assign() : _merge();

                                //Enable back the View Mode
                                pricingSection.viewMode = true;

                                //Enable the Data
                                self.availability.isRatesNotAvailable = false;
                            }
                            catch (e) { throw e; }
                        });
                }
                catch (e) { throw e; }
            },

            //To Save Teaching Styles of a Pro
            teachingstyle: function (professional: any, teachingSection: any) {
                try {
                    var _referenceObj = {
                        "expertiselevels": [
                            {
                                "id": (self.professional.teachingstyle.expertiselevels.length == 0) ? 0 : 1,
                                "expertiseid": 0
                            }
                        ],
                        "agelevels": [
                            {
                                "id": (self.professional.teachingstyle.agelevels.length == 0) ? 0 : 1,
                                "agelevelid": 0
                            }
                        ],
                        "accolades": [],
                        "notableplayerscoached": [],
                        "pgacertified": true
                    };

                    //Set Expertise
                    //_referenceObj.expertiselevels[0].expertiseid = parseInt(teachingSection.temp.expertiseid);
                    _referenceObj.expertiselevels[0] = Object.assign(self.professional.teachingstyle.expertiselevels[0], { expertiseid: teachingSection.temp.expertiselevels });
                    var _arr: any = [];
                    self.expertise.forEach(function (val: any, ind: number) {
                        (val.id == teachingSection.temp.expertiselevels) ? _arr.push(val) : "";
                    })
                    teachingSection.temp.expertiselevels = _arr;


                    //Set Age Levels
                    //_referenceObj.agelevels[0].agelevelid = parseInt(teachingSection.temp.agelevels);
                    _referenceObj.agelevels[0] = Object.assign(self.professional.teachingstyle.agelevels[0], { agelevelid: teachingSection.temp.agelevels });
                    var _arr: any = [];
                    self.ageLevels.forEach(function (val: any, ind: number) {
                        (val.id == teachingSection.temp.agelevels) ? _arr.push(val) : "";
                    })
                    teachingSection.temp.agelevels = _arr;

                    //Set PGA 
                    _referenceObj.pgacertified = teachingSection.temp.pgacertified;

                    //Set Accolades
                    _referenceObj.accolades = teachingSection.temp.accolades;

                    //Set Notable Players Coached
                    _referenceObj.notableplayerscoached = teachingSection.temp.notableplayerscoached;

                    let _assign: any = function () {
                        //Assign Values to Main Reference
                        professional.teachingstyle = teachingSection.temp.teachingstyle;
                        professional.accolades = teachingSection.temp.accolades;
                        professional.notableplayerscoached = teachingSection.temp.notableplayerscoached;

                        professional.pgacertified = teachingSection.temp.pgacertified;
                    }

                    let _merge: any = function () {
                        //Assign Values to Main Reference
                        professional.teachingstyle = Object.assign(professional.teachingstyle, teachingSection.temp);
                        professional.accolades = Object.assign(professional.accolades, teachingSection.temp.accolades);
                        professional.notableplayerscoached = Object.assign(professional.notableplayerscoached, teachingSection.temp.notableplayerscoached);

                        professional.pgacertified = teachingSection.temp.pgacertified;
                    }

                    //Make HTTP Call to save data
                    self._httpService.updateProTeachingDetails(professional.id, _referenceObj)
                        .then((res: any, msg: any) => {
                            try {

                                //Check the State
                                (professional.teachingstyle === null) ? _assign() : _merge();

                                //Enable back the View Mode
                                teachingSection.viewMode = true;

                                //Enable the Data
                                self.availability.isExpertiseNotAvailable = false;
                                self.availability.isAgeLevelsNotAvailable = false;
                                self.availability.isAccoladesNotAvailable = false;
                                self.availability.isNotableplayerscoachedNotAvailable = false;
                            }
                            catch (e) { throw e; }
                        });
                }
                catch (e) { throw e; }
            }
        };

        this.editDetails = {


            basicInfo: function () {
                try {

                    self.userSection.viewMode = !self.userSection.viewMode;

                    self.userSection.temp = self.professional;
                }
                catch (e) { throw e; }
            },

            bankInfo: function () {
                try {

                    self.bankingSection.viewMode = !self.bankingSection.viewMode;

                    self.bankingSection.temp = (self.professional.bankinginfo.length == 0) ? {} : self.professional.bankinginfo;
                }
                catch (e) { throw e; }
            },

            priceInfo: function () {
                try {
                    self.pricingSection.viewMode = !self.pricingSection.viewMode;

                    self.pricingSection.temp = (self.professional.rates === null) ? {} : self.professional.rates;
                }
                catch (e) { throw e; }
            },

            teachingstyle: function () {
                try {

                    self.teachingStyleSection.viewMode = !self.teachingStyleSection.viewMode;

                    self.teachingStyleSection.temp.expertiselevels = (self.professional.teachingstyle.expertiselevels.length == 0) ? self.expertise[0].id : self.professional.teachingstyle.expertiselevels[0].id;
                    self.teachingStyleSection.temp.agelevels = (self.professional.teachingstyle.agelevels.length == 0) ? self.ageLevels[0].id : self.professional.teachingstyle.agelevels[0].id;
                    self.teachingStyleSection.temp.pgacertified = self.professional.pgacertified;


                    self.teachingStyleSection.temp.accolades = (self.professional.accolades.length == 0) ? [{ "id": 0, "title": "" }] : self.professional.accolades;
                    self.teachingStyleSection.temp.notableplayerscoached = (self.professional.notableplayerscoached.length == 0) ? [{ "id": 0, "firstname": "", "lastname": "" }] : self.professional.notableplayerscoached;
                }
                catch (e) { throw e; }
            }
        }
    }
}
