﻿/*
 * Component  : LogIn/SignIn
 * Created by : Niranjan V
 * Created on : 14th May 2018
 */

//Imports
import { Component, NgModule, Injectable, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

//Services
import { LoginService } from './login.service';
import { HttpService } from '../services/app.http.service';
import { ClassService } from '../services/app.classes.service';
import { SessionService } from '../services/app.session.service';
import { CryptoService } from '../services/app.crypto.service';

//Component
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    providers: [
        SessionService, HttpService, ClassService, CryptoService,               //Global Services
        LoginService                                                            //Local Services
    ]
})

//Login Class
export class LoginComponent implements OnInit {

    //Variable Declaration
    public signUpModel: any = {};
    public signInModel: any = {};
    private errorHandler: any = function (error: string) {

        try {
            //Error List
            let _handler: any = {
                "Object reference not set to an instance of an object.": function () { return "Network Busy, Please try again later."; },
                "One or more errors occurred. (An error occurred while sending the request.)": function () { return "Network Busy, Please try again later."; }
            }

            //Return
            return (_handler[error]() == undefined) ? error : _handler[error]();
        }
        catch (e) { return error; }
    };

    //Function Declaration
    public signIn = function (): any { };
    public signUp = function (): any { };

    //Constructor
    constructor(
        private _router: Router,
        private _httpService: HttpService,
        private _classService: ClassService,
        private _cryptoService: CryptoService,
        private _loginService: LoginService) {

        //Assignment
        this.signInModel = _classService.getSignPro();
        this.signUpModel = _classService.getSignPro();
    }

    //On Init
    ngOnInit() {

        //Assignment
        this.signIn = function (): any {

            //Initialize
            var _error: any = document.getElementById('error-handler-sign-in');
            var _formBody: any = document.getElementsByClassName('form-body-in')[0];

            //Validate
            !(_error.className.includes("hide")) ? _error.classList.add("hide") : "";

            //Encryption of Password
            this.signInModel.password = this._cryptoService.encrypt(this.signInModel.password);

            //Sign In
            this._httpService.signIn(this.signInModel)
                .then((res: any, msg: any) => {
                    try {

                        var _OnError = function (signInModel: any, loginService: any, errorHandler: any): any {
                            try {

                                //Enable Error message
                                _error.classList.remove("hide");
                                _error.innerHTML = "Error : " + errorHandler(res.exception);

                                //Clear Texts
                                //signInModel = loginService.setObjectNull(signInModel);

                                //Modify Form Body                                
                                _formBody.classList.add("sign-in");
                            }
                            catch (e) { throw e; }
                        };

                        var _OnSuccess = function (router: any): any {
                            try {

                                //Navigate to Profile on Success
                                router.navigate(['/Profile']);
                            }
                            catch (e) { throw e; }
                        };

                        !(res.exception) ? _OnSuccess(this._router) : _OnError(this.signInModel, this._loginService, this.errorHandler);
                        //this._router.navigate(['/Profile']);
                    }
                    catch (e) { throw e; }
                });

            //Authenticate with Token return
            //Alert error message
        };

        this.signUp = function (): any {

            //Initialize
            var _error: any = document.getElementById('error-handler-sign-up');
            var _formBody: any = document.getElementsByClassName('form-body-up')[0];

            //Validate
            !(_error.className.includes("hide")) ? _error.classList.add("hide") : "";

            //Encryption of Password
            this.signUpModel.password = this._cryptoService.encrypt(this.signUpModel.password);

            //Is the User Valid
            this._httpService.isValidUser(this.signUpModel.email)
                .then((res: any, msg: any) => {
                    try { console.log(res); }
                    catch (e) { throw e; }
                });

            //Sign In
            this._httpService.signUp(this.signUpModel)
                .then((res: any, msg: any) => {
                    try {
                        console.log(res);
                        var _OnError = function (signUpModel: any, loginService: any, errorHandler: any): any {
                            try {

                                //Enable Error message
                                _error.classList.remove("hide");
                                _error.innerHTML = "Error : " + errorHandler(res.exception);

                                //Clear Texts
                                //signUpModel = loginService.setObjectNull(signUpModel);

                                //Modify Form Body                                
                                _formBody.classList.add("sign-up");
                            }
                            catch (e) { throw e; }
                        };

                        var _OnSuccess = function (router: any): any {
                            try {

                                //Navigate to Profile on Success
                                router.navigate(['/Profile']);
                            }
                            catch (e) { throw e; }
                        };

                        !(res.exception) ? _OnSuccess(this._router) : _OnError(this.signUpModel, this._loginService, this.errorHandler);
                    }
                    catch (e) { throw e; }
                });

            //Authenticate with Token return            
            //Alert error message
        };
    }
}

//#region Classes
//#endregion


