﻿/*
 * Service    : Login
 * Created by : Niranjan V
 * Created on : 15th May 2018
 */

//Imports
import { Injectable } from '@angular/core';
import { forEach } from '@angular/router/src/utils/collection';

@Injectable()
//Class
export class LoginService {

    //Variable Declaration
    public iteration: any = { "in": "in", "of": "of" };
    public getValues: any = function (array: any, key: string, type: string): any { };
    public setObjectNull: any = function (object: any) { };

    private defaultVal: any = function (type: string) {

        //Validate Input
        if (typeof type !== 'string') throw new TypeError('Type must be a string.');

        //Handle simple types (primitives and plain function/object)
        switch (type) {
            case 'boolean': return false;
            case 'function': return function () { };
            case 'null': return null;
            case 'number': return 0;
            case 'object': return {};
            case 'string': return "";
            case 'symbol': return Symbol();
            case 'undefined': return void 0;
        }

        try {

            //Look for constructor in this or current scope
            var ctor = typeof [type] === 'function' ? [type] : eval(type);

            return new ctor;

            //Constructor not found, return new object
        } catch (e) { throw e; }
    }

    //Constructor
    constructor() {

        //Assignment
        this.getValues = function (array: any, key: any, type: string): any {
            try {
                let _arr: any[] = [];

                let _handler: any = {

                    "in": function () {
                        //Id
                        var _id = 1;

                        //Iteration
                        for (var obj in array) {
                            _arr.push({ id: _id, key: obj });
                            _id = _id + 1;
                        };
                    },
                    "of": function () {
                        //Iteration
                        for (var obj of array) {
                            _arr.push({ id: obj.id, key: obj[key] });
                        };
                    }
                };

                //Handler
                _handler[type]();

                //Return
                return _arr;
            }
            catch (e) { throw e; }
        };

        this.setObjectNull = function (object: any) {
            try {

                //Clear Texts
                for (var key in object) {
                    if (object.hasOwnProperty(key)) {
                        object[key] = this.defaultVal(typeof object[key]);
                    }
                }

                return object;
            }
            catch (e) { throw e; }
        };
    }
}