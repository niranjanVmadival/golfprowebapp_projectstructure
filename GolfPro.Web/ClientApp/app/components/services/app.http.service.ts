﻿/*
 * Service  : HTTP Calls
 * Created by : Niranjan V
 * Created on : 15th May 2018
 */

//Imports
import { Injectable, Inject, OnInit } from '@angular/core'
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

//Services
import { ClassService } from './app.classes.service';
import { CryptoService } from './app.crypto.service';
import { forEach } from '@angular/router/src/utils/collection';

//import * as CryptoJS from 'crypto-js';

@Injectable()
//Class
export class HttpService {

    //Variable Declarations
    //URLs
    private _exposedUrls: Urls;
    private _httpUrls = {

        //#region SignIn
        "SignUp": "Account/ProSignUp",
        "SignIn": "Account/ProSignIn",
        //#endregion

        //#region Profile
        "GetProDetails": "Profile/GetProDetails",
        "GetExpertise": "Profile/GetExpertise",
        "GetAgeLevels": "Profile/GetAgeLevels",
        "UpdateProUserDetails": "Profile/UpdateProUserDetails",
        "UpdateProBankingDetails": "Profile/UpdateProBankingDetails",
        "UpdateProPriceDetails": "Profile/UpdateProPriceDetails",
        "UpdateProTeachingDetails": "Profile/UpdateProTeachingDetails",
        //#endregion

        //#region SupremeGolf
        "GetSearchedCities": ["SupremeGolf/GetSearchedCities?queryString=", "&limit="],
        "GetCourses": ["SupremeGolf/GetCourses?pageNo=", "&pageLimit="]
        //#endregion
    };

    //To Make Get Calls
    private _get: any;
    private _post: any;
    private _put: any;

    //Header
    private config: any = {
        headers: {
            'accept': 'application/json',
        }
    };
    private supremeGolfConfig: any = {
        headers: {
            'accept': 'application/json',
            "x-api-key": "3aa5397f-4de9-4dc3-9a04-ef92577832bb"
        }
    };

    //#region SignIn
    public signUp = function (signPro: any): any { };
    public signIn = function (signPro: any): any { };
    //#endregion

    //#region Profile
    public getProDetails = function (): any { };
    public getExpertise = function (): any { };
    public getAgeLevels = function (): any { };
    public updateProUserDetails = function (userInformation: any): any { };
    public updateProBankingDetails = function (userId: number, bankingInformation: any): any { }
    public updateProPriceDetails = function (userId: number, priceInformation: any): any { }
    public updateProTeachingDetails = function (userId: number, teachingInformation: any): any { }
    //#endregion

    //#region Exposed URLs
    public getCities = function (query: string): any { };
    public isValidUser = function (email: string): any { };
    //#endregion

    //#region SupremeGolf
    public GetSearchedCities = function (query: string): any { };
    public GetCountries = function (): any { };
    public GetCourses = function (): any { };
    //#endregion

    //Constructor
    constructor(private _http: Http,
        private _classService: ClassService,
        private _cryptoService: CryptoService,
        @Inject('BASE_URL') _baseUrl: string) {

        //#region Exposed URLs
        this._exposedUrls = {

            //SupremeGolf Market Place
            "Cities": ["https://staging.supremegolf.com/api/v4/search/autocomplete?q=", "&limit=", "&include_zip_codes"],
            "ValidUser": ["https://staging.supremegolf.com/api/v4/users/valid?email="],
            "Countries": "https://staging.supremegolf.com:443/api/v4/status/countries",
            "Courses": ["https://staging.supremegolf.com:443/api/v4/courses?pageNo=", "&per_page="]
        }
        //#endregion

        //Assignment
        //#region Generic Methods

        //Get Method
        this._get = function (url: string, config: any): any {
            try {
                var promise = new Promise((resolve, reject) => {
                    this._http.get(url, config)
                        .toPromise()
                        .then(res => { resolve(res.json()); }, msg => { reject(msg); })
                        .catch();
                });

                //Return
                return promise;
            }
            catch (e) { throw e; }
        };

        //Post Method
        this._post = function (Url: string, data: any): any {
            try {
                var promise = new Promise((resolve, reject) => {
                    this._http.post(Url, data)
                        .toPromise()
                        .then(res => { resolve(res.json()); }, msg => { reject(msg); })
                        .catch();
                });

                //Return
                return promise;
            }
            catch (e) { throw e; }
        };

        //Put Method
        this._put = function (Url: string, data: any): any {
            try {
                var promise = new Promise((resolve, reject) => {
                    this._http.put(Url, data)
                        .toPromise()
                        .then(res => { resolve(res.json()); }, msg => { reject(msg); })
                        .catch();
                });

                //Return
                return promise;
            }
            catch (e) { throw e; }
        };
        //#endregion

        //#region SignIn
        this.signUp = function (signPro: any): any {
            try {
                signPro.password = _cryptoService.decrypt(signPro.password);
                return this._post((_baseUrl + this._httpUrls["SignUp"]), signPro);
            }
            catch (e) { throw e; }
        }
        this.signIn = function (signPro: any): any {
            try {
                signPro.password = _cryptoService.decrypt(signPro.password);
                return this._post((_baseUrl + this._httpUrls["SignIn"]), signPro);
            }
            catch (e) { throw e; }
        }
        //#endregion

        //#region Profile
        this.getProDetails = function (): any {
            try { return this._get((_baseUrl + this._httpUrls["GetProDetails"]), this.config); }
            catch (e) { throw e; }
        };
        this.getExpertise = function (): any {
            try { return this._get((_baseUrl + this._httpUrls["GetExpertise"]), this.config); }
            catch (e) { throw e; }
        };
        this.getAgeLevels = function (): any {
            try { return this._get((_baseUrl + this._httpUrls["GetAgeLevels"]), this.config); }
            catch (e) { throw e; }
        };
        this.updateProUserDetails = function (userInformation: any): any {
            try { return this._put((_baseUrl + this._httpUrls["UpdateProUserDetails"]), userInformation); }
            catch (e) { throw e; }
        }
        this.updateProBankingDetails = function (userId: number, bankingInformation: any): any {
            try { return this._put((_baseUrl + this._httpUrls["UpdateProBankingDetails"] + "?userID=" + userId), bankingInformation); }
            catch (e) { throw e; }
        }
        this.updateProPriceDetails = function (userId: number, priceInformation: any): any {
            try { return this._put((_baseUrl + this._httpUrls["UpdateProPriceDetails"] + "?userID=" + userId), priceInformation); }
            catch (e) { throw e; }
        }
        this.updateProTeachingDetails = function (userId: number, teachingInformation: any): any {
            try { return this._put((_baseUrl + this._httpUrls["UpdateProTeachingDetails"] + "?userID=" + userId), teachingInformation); }
            catch (e) { throw e; }
        }
        //#endregion

        //#region SupremeGolf
        this.GetSearchedCities = function (query: string): any {
            try {
                let _concat = "";
                let _params = [query, 14];
                let _http = this._httpUrls["GetSearchedCities"];

                //Iteration
                for (var i = 0; i < _http.length; i++) { _concat += _http[i] + _params[i]; }

                return this._get(_concat, this.config);
            }
            catch (e) { throw e; }
        };
        this.GetCountries = function (): any {
            try { return this._get(this._exposedUrls["Countries"], this.config); }
            catch (e) { throw e; }
        };
        this.GetCourses = function (): any {
            try {
                let _concat = "";
                let _params: any = [1, 100];
                let _http = this._httpUrls["GetCourses"];

                //Iteration
                for (var i = 0; i < _http.length; i++) { _concat += _http[i] + _params[i]; }

                return this._get(_concat, this.config);
            }
            catch (e) { throw e; }
        };
        //#endregion

        //#region Exposed URLs
        this.getCities = function (query: string): any {
            try {
                let _concat = "";
                let _params = [query, 14, ""];
                let _http = this._exposedUrls["Cities"];

                //Iteration
                for (var i = 0; i < _http.length; i++) { _concat += _http[i] + _params[i]; }

                return this._get(_concat, this.config);
            }
            catch (e) { throw e; }
        }
        this.isValidUser = function (email: string): any {
            try {
                let _concat = "";
                let _params = [email];
                let _http = this._exposedUrls["ValidUser"];

                //Iteration
                for (var i = 0; i < _http.length; i++) { _concat += _http[i] + _params[i]; }

                return this._get(_concat, this.supremeGolfConfig);
            }
            catch (e) { throw e; }
        }
        //#endregion
    }
}

interface Urls {
    [key: string]: any
}


