﻿/*
 * Service  : CryptoService Service
 * Created by : Niranjan V
 * Created on : 21st May 2018
 */

//Imports
import { Injectable, OnInit } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Injectable()
//Class
export class CryptoService {

    //Variable Declarations
    private key: any = CryptoJS.enc.Utf8.parse('7061737323313233');
    private iv: any = CryptoJS.enc.Utf8.parse('7061737323313233');
    private config: any = {
        keySize: 128 / 8,
        iv: this.iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    };

    //Function Declaration
    public encrypt: any = function (): any { };
    public decrypt: any = function (): any { };

    //Constructor
    constructor() {

        //Assignment
        this.encrypt = function (text: string) { return CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(text), this.key, this.config); }
        this.decrypt = function (encrypted: any) { return CryptoJS.AES.decrypt(encrypted, this.key, this.config).toString(CryptoJS.enc.Utf8); }
    }

}