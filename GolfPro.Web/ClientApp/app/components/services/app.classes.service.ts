﻿/*
 * Service  : Classes
 * Created by : Niranjan V
 * Created on : 17th May 2018
 */

//Imports
import { Injectable, OnInit } from '@angular/core'
import { HttpService } from './app.http.service';
import { Router } from '@angular/router';

@Injectable()
//Class
export class ClassService {

    //Variable Declarations

    //#region Profile
    public getSignPro = function (): any { };
    public getProfessional = function (): any { };
    //#endregion

    //Constructor
    constructor() {

        //Assignment
        this.getSignPro = function () { return new SignPro(); };
        this.getProfessional = function () { return new Professional(); };
    }
}

//#region SignPro
class SignPro {

    //Variable Declaration
    public firstname: string | undefined;
    public lastname: string | undefined;
    public email: string | undefined;
    public password: string | undefined;
    public role: number = 1;
    public zip: number | undefined;

    //Constructor
    constructor() {

        //Assignment
    }
}
//#endregion

//#region Professional
class Personal {
    public firstname: string | undefined;
    public lastname: string | undefined;
    public email: string | undefined;
    public phone: string | undefined;
    public description: string | undefined;

    //Constructor
    constructor() {

        //Assignment
    }
}
class Address {

    //Variable Declaration
    public address_1: string | undefined;
    public address_2: string | undefined;
    public city: string | undefined;
    public state: string | undefined;
    public zip: string | undefined;
    public country: number | undefined;

    //Constructor
    constructor() {
        //Assignment
    }
}
class PreferredExpertiseInfo {
    //Variable Declaration
    public id: number | undefined;
    public title: number | undefined;
    public description: string | undefined;

    //Constructor
    constructor() {

        //Assignment
    }
}
class PreferredAgeLevelInfo {
    //Variable Declaration
    public id: number | undefined;
    public level: number | undefined;
    public description: string | undefined;

    //Constructor
    constructor() {

        //Assignment
    }
}
class TeachingStyle {
    //Variable Declaration
    public expertiselevels: PreferredExpertiseInfo[];
    public agelevels: PreferredAgeLevelInfo[];

    //Constructor
    constructor() {

        //Application
        this.expertiselevels = [new PreferredExpertiseInfo()];
        this.agelevels = [new PreferredAgeLevelInfo()];
    }
}
class BankInfo {
    //Variable Declaration
    public accounttypeId: number | undefined;
    public accholdername: string | undefined;
    public accounttype: string | undefined;
    public accholderfirstname: string | undefined;
    public accholderlastname: string | undefined;
    public accountnumber: number | undefined;
    public routingnumber: number | undefined;
    public status: string | undefined;

    //Constructor
    constructor() {

        //Assignment
    }
}
class AccoladeInfo {
    //Variable Declaration
    public id: number | undefined;
    public title: number | undefined;

    //Constructor
    constructor() {

        //Assignment
    }
}
class NotablePlayersCoachedInfo {

    //Variable Declaration
    public id: number | undefined;
    public firstname: string | undefined;
    public lastname: string | undefined;

    //Constructor
    constructor() {

        //Assignment
    }
}
class PreferredPricingInfo {
    //Variable Declaration
    public id: number | undefined;
    public individual: number | undefined;
    public group: number | undefined;
    public package: number | undefined;
    public currencyinfoid: number | undefined;
    public currencyinfo: CurrencyInfo;

    //Constructor
    constructor() {

        //Assignment
        this.currencyinfo = new CurrencyInfo();
    }
}
class CurrencyInfo {
    //Variable Declaration
    public currencyinfoid: number | undefined;
    public country: string | undefined;
    public currency: string | undefined;

    //Constructor
    constructor() {

        //Assignment
    }
}
//class Address {
//    //Variable Declaration

//    //Constructor
//    constructor() { }
//}
class Professional {
    //Variable Declaration
    public id: number | undefined;
    public courseid: number | undefined;
    public dob: string | undefined;
    public avatar: string | undefined;
    public gender: string | undefined;
    public pgacertified: boolean | undefined;

    public accolades: AccoladeInfo[];
    public notableplayerscoached: NotablePlayersCoachedInfo[];
    public personal: Personal;
    public address: Address;
    public teachingstyle: any;
    public bankinginfo: BankInfo[];
    public rates: any;

    //Constructor
    constructor() {

        //Initialize
        this.personal = new Personal();
        this.address = new Address();
        this.teachingstyle = {
            expertiselevels: [],
            agelevels: []
        };
        this.accolades = [];
        this.notableplayerscoached = [];
        this.bankinginfo = [];
        this.rates = null;
    }
}
//#endregion