/*
 * Module  : Server
 * Created by : Niranjan V
 * Created on : 17th May 2018
 */

//Imports
import { NgModule } from '@angular/core';
import { ServerModule } from '@angular/platform-server';
import { AppModuleShared } from './app.shared.module';
import { AppComponent } from './components/app/app.component';

//Module
@NgModule({
    bootstrap: [AppComponent],
    imports: [
        ServerModule,
        AppModuleShared
    ]
})

//Class
export class AppModule { }
