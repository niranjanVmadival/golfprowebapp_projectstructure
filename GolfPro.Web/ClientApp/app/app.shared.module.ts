
//Angular Components
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Router } from '@angular/router';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

//Application User Defined Components
import { AppComponent } from './components/app/app.component';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';

//Application User Defined Services
import { SessionService } from './components/services/app.session.service';
import { HttpService } from './components/services/app.http.service';
import { ClassService } from './components/services/app.classes.service'
import { CryptoService } from './components/services/app.crypto.service';

//#region Global Variables

//Navigation
const APP_NAVIGATION: any = [

    //Login
    { path: 'Login', component: LoginComponent },

    //Profile
    { path: 'Profile', component: ProfileComponent },

    //Defaults
    { path: '**', redirectTo: 'Login' },
    { path: '', redirectTo: 'Login', pathMatch: 'full' }
]
//#endregion

//Modules
@NgModule({
    declarations: [

        //App
        AppComponent,

        //Login
        LoginComponent,

        //Profile
        ProfileComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        RouterModule.forRoot(APP_NAVIGATION),
        AngularFontAwesomeModule
    ],
    providers: [SessionService, HttpService, ClassService, CryptoService]
})

//Class
export class AppModuleShared {

    //Constructor
    constructor(private router: Router) {
        router.navigate(['/login']);
    }
}
