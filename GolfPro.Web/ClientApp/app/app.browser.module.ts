/*
 * Module  : Browser
 * Created by : Niranjan V
 * Created on : 17th May 2018
 */


//Imports
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppModuleShared } from './app.shared.module';
import { AppComponent } from './components/app/app.component';

//#region Function
export function getBaseUrl() {
    return document.getElementsByTagName('base')[0].href;
}
//#endregion

//Module
@NgModule({
    bootstrap: [AppComponent],
    imports: [
        BrowserModule,
        AppModuleShared
    ],
    providers: [
        { provide: 'BASE_URL', useFactory: getBaseUrl }//() => { return document.getElementsByTagName('base')[0].href;} }
    ]
})

//Class
export class AppModule {
}
