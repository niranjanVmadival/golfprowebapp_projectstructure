﻿module.exports = function (grunt) {
    grunt.initConfig({
        clean: [],
        concat: {
            all: {
                src: [],
                dest: 'concat.js'
            }
        },
        jshint: {
            files: [],
            options: {
                '-W069': false,
            }
        },
        uglify: {
            all: {
                src: [],
                dest: '' //'wwwroot/lib/combined.min.js'
            }
        },
        watch: {
            files: ["TypeScript/*.js"],
            tasks: ["all"]
        }
    });
    grunt.file.setBase('../Grunt/');
    //grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks('grunt-contrib-jshint');
    //grunt.loadNpmTasks('grunt-contrib-concat');
    //grunt.loadNpmTasks('grunt-contrib-uglify');
    //grunt.loadNpmTasks('grunt-contrib-watch');
    //grunt.registerTask("all", ['clean', 'concat', 'jshint', 'uglify']);
}