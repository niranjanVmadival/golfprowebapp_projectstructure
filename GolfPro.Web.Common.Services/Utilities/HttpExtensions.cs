﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace GolfPro.Web.Common.Services.Utilities
{
    public static class HttpClientExtensions
    {
        /// <summary>
        /// To Post a Sync Call as JSON
        /// </summary>
        /// <typeparam name="T">Any Object</typeparam>
        /// <param name="httpClient"></param>
        /// <param name="url"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Task<HttpResponseMessage> PostAsJsonAsync<T>(this HttpClient httpClient, string url, T data)
        {
            try
            {
                //Serialize Content
                var _content = new StringContent(JsonConvert.SerializeObject(data));

                //Set Headers
                _content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                //Return
                return httpClient.PostAsync(url, _content);
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// To Put a Async Call as JSON
        /// </summary>
        /// <typeparam name="T">Any Object</typeparam>
        /// <param name="httpClient"></param>
        /// <param name="url"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Task<HttpResponseMessage> PutJsonAsync<T>(this HttpClient httpClient, string url, T data)
        {
            try
            {
                //Serialize Content
                var _content = new StringContent(JsonConvert.SerializeObject(data));

                //Set Headers
                _content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                //Return
                return httpClient.PutAsync(url, _content);
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// To Read a Sync Call as JSON
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="content"></param>
        /// <returns></returns>
        public static async Task<T> ReadAsJsonAsync<T>(this HttpContent content)
        {
            try { return JsonConvert.DeserializeObject<T>(await content.ReadAsStringAsync()); }
            catch (Exception ex) { throw ex; }
        }
    }
}
