﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace GolfPro.Web.Common.Services.Utilities
{
    public static class StringExtensions
    {
        public static string AddQueryParam(this string source, string key, string value)
        {
            string delim;
            if ((source == null) || !source.Contains("?"))
            {
                delim = "?";
            }
            else if (source.EndsWith("?") || source.EndsWith("&"))
            {
                delim = string.Empty;
            }
            else
            {
                delim = "&";
            }

            return source + delim + HttpUtility.UrlEncode(key)
                + "=" + HttpUtility.UrlEncode(value);
        }
    }
}
