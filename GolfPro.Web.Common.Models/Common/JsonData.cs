﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Common.Models.Common
{
    public class JsonData
    {
        #region Attributes
        public object Object { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; } 
        #endregion
    }
}
