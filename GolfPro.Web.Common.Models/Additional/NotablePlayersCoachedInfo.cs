﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Common.Models.Additional
{
    public class NotablePlayersCoachedInfo
    {
        #region Constructor
        public NotablePlayersCoachedInfo()
        {

        }
        #endregion

        #region Attributes
        [JsonProperty("id")]
        public int NotablePlayersCoachedID { get; set; }
        [JsonProperty("firstname")]
        public string FirstName { get; set; }
        [JsonProperty("lastname")]
        public string LastName { get; set; }
        #endregion
    }
}
