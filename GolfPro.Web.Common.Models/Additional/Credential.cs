﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Common.Models.Additional
{
    public class Credential
    {
        #region Constructor
        public Credential()
        {

        }
        #endregion

        #region Attributes
        public string Email { get; set; }
        public string Password { get; set; }
        #endregion
    }
}
