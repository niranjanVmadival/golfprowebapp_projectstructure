﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Common.Models.Additional
{
    public class CurrencyInfo
    {
        #region Constructor
        public CurrencyInfo()
        {

        }
        #endregion

        #region Attributes
        [JsonProperty("currencyinfoid")]
        public int CurrencyInfoID { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("currency")]
        public string Currency { get; set; }
        #endregion
    }
}
