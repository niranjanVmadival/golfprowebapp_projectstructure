﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Common.Models.Additional
{
    public class TeachingStyleInfo
    {
        #region Constructor
        public TeachingStyleInfo()
        {
            ExpertiseLevels = new List<PreferredExpertiseInfo>();
            AgeLevels = new List<PreferredAgeLevelInfo>();
        }
        #endregion

        #region Attributes
        [JsonProperty("expertiselevels")]
        public List<PreferredExpertiseInfo> ExpertiseLevels { get; set; }
        [JsonProperty("agelevels")]
        public List<PreferredAgeLevelInfo> AgeLevels { get; set; }
        #endregion
    }
}
