﻿using GolfPro.Web.Common.Models.Profile.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Common.Models.Additional
{
    public class PreferredAgeLevelInfo
    {
        #region Constructor
        public PreferredAgeLevelInfo()
        {

        }
        #endregion

        #region Attributes
        [JsonProperty("id")]
        public int PreferredAgeLevelID { get; set; }
        [JsonProperty("agelevelid")]
        public int AgeLevelId { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("agelevel")]
        public AgeLevelDTO AgeLevel { get; set; }
        #endregion
    }
}
