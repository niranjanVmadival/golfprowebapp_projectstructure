﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Common.Models.Additional
{
    public class AccoladeInfo
    {
        #region Constructor
        public AccoladeInfo()
        {

        }
        #endregion

        #region Attributes
        [JsonProperty("id")]
        public int AccoladeID { get; set; }
        [JsonProperty("title")]
        public string Name { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        #endregion
    }
}
