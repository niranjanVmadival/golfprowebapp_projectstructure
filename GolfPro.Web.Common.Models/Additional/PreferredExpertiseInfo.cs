﻿using GolfPro.Web.Common.Models.Profile.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Common.Models.Additional
{
    public class PreferredExpertiseInfo
    {
        #region Constructor
        public PreferredExpertiseInfo()
        {
            
        }
        #endregion

        #region Attributes
        [JsonProperty("id")]
        public int PreferredExpertiseID { get; set; }
        [JsonProperty("expertiseid")]
        public int ExpertiseID { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("expertise")]
        public ExpertiseDTO Expertise { get; set; }
        #endregion
    }
}
