﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Common.Models.Additional
{
    public class PreferredPricingInfo
    {
        #region Constructor
        public PreferredPricingInfo()
        {
            CurrencyInfo = new CurrencyInfo();
        }
        #endregion

        #region Attributes
        [JsonProperty("id")]
        public int PreferredPricingID { get; set; }
        [JsonProperty("individual")]
        public double Individual { get; set; }
        [JsonProperty("group")]
        public double Group { get; set; }
        [JsonProperty("package")]
        public double Package { get; set; }
        [JsonProperty("currencyinfoid")]
        public int CurrencyInfoID { get; set; }
        [JsonProperty("currencyinfo")]
        public CurrencyInfo CurrencyInfo { get; set; }
        #endregion
    }
}
