﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Common.Models.Additional
{
    public class AddressInfo
    {
        #region Constructor
        public AddressInfo()
        {

        }
        #endregion

        #region Attributes
        [JsonProperty("address_1")]
        public string Address1 { get; set; }
        [JsonProperty("address_2")]
        public string Address2 { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
        [JsonProperty("zip")]
        public string Zipcode { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        #endregion
    }
}
