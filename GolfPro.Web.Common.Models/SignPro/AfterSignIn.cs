﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Common.Models.SignPro
{
    public class AfterSignIn
    {
        #region Constructor
        public AfterSignIn()
        {

        }
        #endregion

        #region Attributes
        public bool tagged { get; set; }
        public string token { get; set; }
        public string role { get; set; }
        public string userid { get; set; }
        #endregion
    }
}
