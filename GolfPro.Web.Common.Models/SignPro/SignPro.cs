﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Common.Models.SignPro
{
    public class SignPro
    {
        #region Constructor
        public SignPro()
        {
            
        }
        #endregion

        #region Attributes
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string zip { get; set; }
        public string password { get; set; }
        public int role { get; set; }
        #endregion
    }
}
