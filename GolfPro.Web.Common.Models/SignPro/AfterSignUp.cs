﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Common.Models.SignPro
{
    public class AfterSignUp
    {
        #region Constructor
        public AfterSignUp()
        {

        }
        #endregion

        #region Attributes
        public string confirmation_url { get; set; }
        public string token { get; set; }
        public string role { get; set; }
        public string userid { get; set; }
        #endregion
    }
}
