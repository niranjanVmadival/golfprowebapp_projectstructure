﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Common.Models.Profile.DTO
{
    public class PreferredAgeLevelDTO
    {
        [JsonProperty("id")]
        public int PreferredAgeLevelID { get; set; }
        [JsonProperty("agelevelid")]
        public int AgeLevelID { get; set; }
    }
}
