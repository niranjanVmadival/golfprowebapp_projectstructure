﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Common.Models.Profile.DTO
{
    public class ExpertiseDTO
    {
        #region Attributes
        [JsonProperty("id")]
        public int ExpertiseID { get; set; }
        [JsonProperty("title")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; } 
        #endregion
    }
}
