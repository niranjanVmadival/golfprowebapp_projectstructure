﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Common.Models.Profile.DTO
{
    public class AgeLevelDTO
    {
        #region Attributes
        [JsonProperty("id")]
        public int AgeLevelID { get; set; }
        [JsonProperty("level")]
        public string Level { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        #endregion
    }
}
