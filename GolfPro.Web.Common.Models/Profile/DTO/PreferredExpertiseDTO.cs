﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Common.Models.Profile.DTO
{
    public class PreferredExpertiseDTO
    {
        [JsonProperty("id")]
        public int PreferredExpertiseID { get; set; }
        [JsonProperty("expertiseid")]
        public int ExpertiseID { get; set; }
    }
}
