﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Common.Models.Profile.DTO
{
    public class ProPersonalDTO
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("personal")]
        public PersonalInfoDTO PersonalInfo { get; set; }
        [JsonProperty("address")]
        public AddressDTO Address { get; set; }
        [JsonProperty("dob")]
        public string DateOfBirth { get; set; }
        [JsonProperty("courseid")]
        public int CourseID { get; set; }
        [JsonProperty("pgacertified")]
        public bool PGACertified { get; set; }
        [JsonProperty("gender")]
        public string Gender { get; set; }
        [JsonProperty("avatar")]
        public string Base64Image { get; set; }
        [JsonProperty("prodescription")]
        public string ProDescription { get; set; }
    }
}
