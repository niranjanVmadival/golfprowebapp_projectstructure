﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Common.Models.Profile.DTO
{
    public class TeachingDTO
    {

        #region Constructor
        public TeachingDTO()
        {

        }
        #endregion

        #region Attributes
        [JsonProperty("expertiselevels")]
        public List<PreferredExpertiseDTO> ExpertiseLevels { get; set; }
        [JsonProperty("agelevels")]
        public List<PreferredAgeLevelDTO> AgeLevels { get; set; }
        public List<AccoladeDTO> Accolades { get; set; }
        [JsonProperty("notableplayerscoached")]
        public List<NotablePlayersCoachedDTO> NotablePlayersCoached { get; set; }
        [JsonProperty("pgacertified")]
        public bool PGACertified { get; set; }
        #endregion
    }
}
