﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Common.Models.Profile.DTO
{
    public class AccoladeDTO
    {
        [JsonProperty("id")]
        public int AccoladeID { get; set; }
        [JsonProperty("title")]
        public string Name { get; set; }
    }
}
