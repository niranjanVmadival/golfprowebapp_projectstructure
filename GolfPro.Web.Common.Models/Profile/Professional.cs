﻿using GolfPro.Web.Common.Models.Additional;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace GolfPro.Web.Common.Models.Profile
{
    public class Professional
    {
        #region Constructor
        public Professional()
        {

        }
        #endregion

        #region Attributes
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("personal")]
        public PersonalInfo PersonalInfo { get; set; }
        [JsonProperty("address")]
        public AddressInfo AddressInfo { get; set; }
        [JsonProperty("dob")]
        public string DateOfBirth { get; set; }
        [JsonProperty("courseid")]
        public int CourseID { get; set; }
        [JsonProperty("teachingstyle")]
        public TeachingStyleInfo TeachingStyle { get; set; }
        [JsonProperty("accolades")]
        public List<AccoladeInfo> Accolades { get; set; }
        [JsonProperty("notableplayerscoached")]
        public List<NotablePlayersCoachedInfo> NotablePlayersCoached { get; set; }
        [JsonProperty("bankinginfo")]
        public List<BankInfo> BankingInfos { get; set; }
        [JsonProperty("rates")]
        public PreferredPricingInfo PreferredPricing { get; set; }
        [JsonProperty("pgacertified")]
        public bool PGACertified { get; set; }
        [JsonProperty("gender")]
        public string Gender { get; set; }
        [JsonProperty("avatar")]
        public string Base64Image { get; set; }
        #endregion
    }
}
